import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { button, buttonOutlined, buttonText, deckTitle, questionCount } from '../utils/styles';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { rose } from '../utils/colors';

class DeckDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    const { title } = navigation.state.params.deck;

    return {
      title
    }
  }

  render() {
    const { deck } = this.props.navigation.state.params;

    return (
      <View style={styles.container}>
        <View style={styles.containerTop}>
          <Text style={styles.deckTitle}>{deck.title}</Text>
          <MaterialCommunityIcons name='cards-outline' size={56} color={rose} style={{marginTop:20, marginBottom:8}} />
          <Text style={styles.questionCount}>{deck.questions.length} {deck.questions.length === 1 ? 'Card' : 'Cards'}</Text>
        </View>
        <View style={styles.containerBottom}>
          <TouchableOpacity style={styles.buttonOutlined} onPress={() => this.props.navigation.navigate('AddCard', { deck })}>
            <Text style={[styles.buttonText, {color: '#222'}]}>Add Card</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.button, {marginTop:10}]} onPress={() => this.props.navigation.navigate('Quiz', { deck })}>
            <Text style={styles.buttonText}>Start Quiz</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
    justifyContent: 'space-between',
  },
  containerTop: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button,
  buttonText,
  buttonOutlined,
  deckTitle,
  questionCount,
});

export default DeckDetail;
