import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { button, buttonText } from '../utils/styles';
import { saveDeckTitle } from '../api';

class NewDeck extends Component {
  state = {
    deckTitle: '',
  }

  submitDeck = (deckTitle) => {
    if (deckTitle.trim() === '') {
      alert('Deck Title cannot be blank!');
    } else {
      saveDeckTitle(deckTitle).then((deck) => {
        this.props.navigation.navigate('DeckDetail', { deck })
      });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <Text style={styles.titleText}>What is the Title of your new Deck?</Text>
          <TextInput
            placeholder="Deck Title"
            style={styles.input}
            value={this.state.deckTitle}
            onChangeText={(deckTitle) => this.setState({ deckTitle })}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={() => this.submitDeck(this.state.deckTitle)}>
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'space-between',
    padding: 25,
  },
  contentContainer: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: 24,
    alignSelf: 'center',
    marginBottom: 25,
    width: 200,
    textAlign: 'center',
  },
  input: {
    borderRadius: 3,
    backgroundColor: '#f2f2f2',
    height: 50,
    paddingHorizontal: 20,
    fontSize: 18,
    marginBottom: 10,
    textAlign: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: .3
  },
  button,
  buttonText,
});

export default NewDeck;
