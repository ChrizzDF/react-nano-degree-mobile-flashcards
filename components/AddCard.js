import React, { Component } from 'react';
import { StyleSheet, Switch, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { button, buttonText } from '../utils/styles';
import { saveCard } from '../api';

class AddCard extends Component {
  state = {
    question: '',
    answer: '',
  }

  submitCard = () => {
    const { answer, question } = this.state;

    if (question.trim() === '' || answer.trim() === '') {
      alert('Question & Answer are required fields.');
    } else {
      saveCard(this.state, this.props.navigation.state.params.deck).then(() => this.props.navigation.navigate('Decks'))
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.contentContainer}>
          <TextInput
            placeholder="Question"
            style={styles.input}
            value={this.state.question}
            onChangeText={(question) => this.setState({ question })}
          />
          <TextInput
            placeholder="Answer"
            style={styles.input}
            value={this.state.answer}
            onChangeText={(answer) => this.setState({ answer })}
          />
        </View>

        <TouchableOpacity style={styles.button} onPress={() => this.submitCard()}>
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'space-between',
    padding: 25,
  },
  contentContainer: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: 24,
    alignSelf: 'center',
    marginBottom: 25,
    width: 200,
    textAlign: 'center',
  },
  input: {
    borderRadius: 3,
    backgroundColor: '#f2f2f2',
    height: 50,
    paddingHorizontal: 20,
    fontSize: 18,
    marginBottom: 10,
    textAlign: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowRadius: 3,
    shadowOpacity: .3
  },
  button,
  buttonText,
});

export default AddCard;
