import React, { Component } from 'react';
import { AsyncStorage, Button, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { decks } from '../utils/mockData';
import { objectToArray } from '../utils/helpers';
import { DECKS_STORAGE_KEY, getDecks } from '../api';
import { deckTitle, questionCount } from '../utils/styles';

class Decks extends Component {
  constructor(props) {
    super(props);

    this.state = {
      decks: {},
    }

    this.shouldReloadData = props.navigation.addListener(
      'willFocus', () => getDecks().then((decks) => this.setState({ decks: objectToArray(JSON.parse(decks)) })),
    );
  }
  
  setMockData = () => {
    return AsyncStorage.setItem(DECKS_STORAGE_KEY, JSON.stringify(decks));
  }

  componentDidMount() {
    this.setMockData().then(() => {
      getDecks().then((decks) => this.setState({ decks: objectToArray(JSON.parse(decks)) }));
    });
  }

  render() {
    const { decks } = this.state;

    return (
      <View style={!decks.length && styles.noDecksLayout}>
        {(decks.length ? decks.map((deck, index) => {
          const { questions, title } = deck;

          return <TouchableOpacity style={styles.deck} key={index} onPress={() => this.props.navigation.navigate('DeckDetail', { deck })}>
            <Text style={styles.deckTitle}>{title}</Text>
            <Text style={styles.questionCount}>{questions.length} {questions.length === 1 ? 'Card' : 'Cards'}</Text>
          </TouchableOpacity>
        }) : (
          <View>
            <Text style={styles.noDecks}>No Decks found.</Text>
            <Text>Why don't you add some?</Text>
            <Button
              title="Create Deck"
              onPress={() => this.props.navigation.navigate('NewDeck')}
            />
          </View>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  deck: {
    borderStyle: 'solid',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#ddd',
    backgroundColor: 'white',
    paddingVertical: 30,
    marginBottom: -1,
  },
  deckTitle,
  questionCount,
  noDecks: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 5,
  },
  noDecksLayout: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});

export default Decks;
