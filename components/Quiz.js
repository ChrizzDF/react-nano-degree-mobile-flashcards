import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { button, buttonOutlined, buttonText, deckTitle, questionCount, questionTitle } from '../utils/styles';
import FlipCard from 'react-native-flip-card';
import { Ionicons } from '@expo/vector-icons';
import { rose } from '../utils/colors';

class Quiz extends Component {
  state = {
    currentQuestion: this.props.navigation.state.params.deck.questions[0],
    correctAnswers: 0,
    quizIsOver: false,
  }

  logAnswer = (answer) => {
    const { questions } = this.props.navigation.state.params.deck;
    const indexOfCurrentQuestion = questions.indexOf(this.state.currentQuestion);
    const nextQuestion = this.props.navigation.state.params.deck.questions[indexOfCurrentQuestion + 1];

    if (answer) {
      this.setState(prevState => ({ correctAnswers: prevState.correctAnswers + 1 }));
    }

    if (nextQuestion) {
      this.setState({ currentQuestion: nextQuestion });
    } else {
      this.setState({ quizIsOver: true });
    }
  }

  getQuizResult = () => {
    const { questions } = this.props.navigation.state.params.deck;
    const percentage = this.state.correctAnswers / questions.length * 100;
    const result = percentage.toFixed(2).replace('.00', '');
    
    return `${result}%`;
  }

  retryQuiz = () => {
    const { deck } = this.props.navigation.state.params;

    this.setState({
      currentQuestion: deck.questions[0],
      correctAnswers: 0,
      quizIsOver: false,
    });

    this.props.navigation.navigate('Quiz', { deck });
  }
  
  render() {
    const { questions } = this.props.navigation.state.params.deck;
    const { currentQuestion } = this.state;

    return (
      <View style={styles.container}>
        {!this.state.quizIsOver &&
          <View>
            <Text>{questions.indexOf(currentQuestion) + 1}/{questions.length}</Text>
          </View>
        }
        {this.state.quizIsOver ? (
          <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
            <Text style={{fontSize:28, fontWeight:'bold', marginBottom:20}}>Quiz completed!</Text>
            <Ionicons name="md-trophy" size={80} color={rose} style={{marginVertical:40}} />
            <Text style={{fontSize:24, fontWeight:'bold', marginBottom:20, color:'green'}}>{this.state.correctAnswers}/{questions.length} <Ionicons name="md-checkmark-circle-outline" size={18}/></Text>
            <Text style={styles.result}>{this.getQuizResult()}</Text>
          </View>
        ) : (
          <View style={styles.flipCardContainer}>
            <FlipCard
              style={{flex:1, minWidth: '100%', borderRadius: 10, borderWidth: 0}}
              friction={6}
              perspective={1000}
              alignHeight={true}
              alignWidth={true}
              flipHorizontal={true}
              flipVertical={false}
            >
              <View style={styles.card}>
                <Text style={styles.questionTitle}>{currentQuestion.question}</Text>
              </View>
              <View style={styles.card}>
                <Text style={styles.answerText}>{currentQuestion.answer}</Text>
              </View>
            </FlipCard>
          </View>
        )}
        {this.state.quizIsOver ? (
          <TouchableOpacity style={[styles.button, {marginTop:10}]} onPress={this.retryQuiz}>
            <Text style={styles.buttonText}>Retry Quiz</Text>
          </TouchableOpacity>
        ) : (
          <View style={styles.containerBottom}>
            <Text style={styles.hint}>Touch Card to reveal Answer</Text>
            <TouchableOpacity style={[styles.button, {backgroundColor:'green'}]} onPress={() => this.logAnswer(true)}>
              <Text style={styles.buttonText}>Correct</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.button, {marginTop:10, backgroundColor:'red'}]} onPress={() => this.logAnswer(false)}>
              <Text style={styles.buttonText}>Incorrect</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
  },
  hint: {
    color: '#999',
    textAlign: 'center',
    marginBottom: 35,
  },
  flipCardContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 15,
    marginBottom: 30,
  },
  answerText: {
    fontSize: 18,
    textAlign: 'center',
  },
  card: {
    backgroundColor: 'white',
    borderRadius: 5,
    padding: 25,
    flex: 1,
    justifyContent: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.3)',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 6,
    shadowOpacity: 1
  },
  result: {
    fontSize:26,
    fontWeight:'bold',
  },
  containerBottom: {
    marginTop: 'auto',
  },
  button,
  buttonText,
  buttonOutlined,
  deckTitle,
  questionCount,
  questionTitle,
});

export default Quiz;
