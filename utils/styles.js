const button = {
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#222',
  height: 50,
  paddingHorizontal: 20,
  borderRadius: 5,
  alignSelf: 'center',
  width: 200,
};

const buttonOutlined = {
  ...button,
  backgroundColor: 'transparent',
  borderWidth: 1,
  borderColor: '#222',
};

const buttonText = {
  color: 'white',
  fontWeight: 'bold',
};

const paddedContainer = {
  padding: 25,
};

const deckTitle = {
  textAlign: 'center',
  color: '#222',
  fontWeight: 'bold',
  fontSize: 20,
};

const questionTitle = {
  textAlign: 'center',
  color: '#222',
  fontWeight: 'bold',
  fontSize: 24,
};

const questionCount = {
  fontSize: 18,
  textAlign: 'center',
  color: '#666',
};

export {
  button,
  buttonOutlined,
  buttonText,
  deckTitle,
  paddedContainer,
  questionCount,
  questionTitle,
};
