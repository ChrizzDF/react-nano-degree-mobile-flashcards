# Mobile Flashcards

### This is my personal implementation of the 3rd Udacity React Nano Degree project named "Mobile Flashcards".

Because this repository is not meant for contributing, you'll only find instructions how to get this application up and running.

## Installation

1. From within your project root run `yarn` to install all the required dependencies.
2. Run the Application by executing `yarn start` where you'll have additional options such as opening the iOS or Android Simulator once the server is up and running.

## Functionality

The minimum required functionality of this Application can be found inside [the Rubric for this project](assets/rubric.pdf).
But there're also additional features like form field validation so you won't be able to create Decks or Card with empty data.

## Implementation Detials

This application is utilizing [AsyncStorage](https://facebook.github.io/react-native/docs/asyncstorage.html) provided by `react-native` to persist data locally.
