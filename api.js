import { AsyncStorage } from 'react-native';

const DECKS_STORAGE_KEY = 'Flashcards:decks';

const getDecks = () => {
  return AsyncStorage.getItem(DECKS_STORAGE_KEY);
}

const getDeck = async (deckTitle) => {
  return AsyncStorage.getItem(DECKS_STORAGE_KEY)
    .then(JSON.parse)
    .then((decks) => decks[deckTitle]);
}

const saveDeckTitle = async (deckTitle) => {
  const newDeck = {
    [deckTitle]: {
      title: deckTitle,
      questions: [],
    }
  };

  AsyncStorage.mergeItem(DECKS_STORAGE_KEY, JSON.stringify(newDeck));

  const createdDeck = await getDeck(deckTitle);

  return createdDeck;
}

const saveCard = async (newCard, deck) => {
  const updatedDeck = {
    [deck.title]: {
      title: deck.title,
      questions: [...deck.questions, newCard],
    }
  };

  return AsyncStorage.mergeItem(DECKS_STORAGE_KEY, JSON.stringify(updatedDeck));
}

export { DECKS_STORAGE_KEY, getDecks, saveCard, saveDeckTitle };
